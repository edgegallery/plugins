# HTTP based North Exporter for profile

## flow of data.
- This north exporter is to get online data from profile
- This exporter read data from internal MQ from profile
- User can regsiter their call back
- And they can provide the Node name which data they are interested like fledge, kuiper etc.
- Sample call registry can be as below

POST https://http-exporter:8094/profile/v1/telemetry/register
Request Body: 
Json object with callback API
{"url":"http://0.0.0.0:9092//telemtry/data",
“node”: “kuiper”
}

- The data is in below format
[{"asset":"Asset-name","readings":{}. "timestamp":"2022-03-08 17:29:31.960061326"}]
- Readings can have multiple data based on device schema.
- User shouldknow how to parse device data coming inside Readings.
 
## Sample Application
- sample reference application to test and expernice north exporter.
- once profile is deployed and profile config job is completed, deloy sample iot app by applying yaml file in sample app folder 
- It register call back to http north exporter with Profile Node which data it is want
- once profile have data on this Node, it will forward to sample application.
- in application, just printing the readings

@Note: 
- Currently data call back from app is http only due to certification managment. It can be enhance in future.
- Callback registry Interface is support https, app need to enable verify none since they will not have exporter CA and it is not distributed since it is self signed.
- For sogno, asset is empty and Readings will have array of data in below sample:
- Power grid app can parse data as per sogno project format inside from Readings.  
[{"asset":"","readings":[{"data":[110000,0,19805.072788436424,-0.033469532598253124,19701.15233192998,-0.03962606776889559,19529.045125789868,-0.0490248772552469,19520.723121942043,-0.049480306034798006,19515.17065978783,-0.049796259532116305,19508.86912499003,-0.050168678776156925,19500.352623208502,-0.050544573302074035,19501.42729578896,-0.05043269332533982,19496.201144185296,-0.05067258201332931,19490.065416962665,-0.050959035814713394,19489.083435543864,-0.05100248343309452,19817.885821435342,-0.028370471199203436,19787.3240798723,-0.029004800556170628,19769.6583993727,-0.029347891214682884],"sequence":153,"ts":{"origin":[1646760570,547551630],"received":[1646760570,547551630]}}],"timestamp":"2022-03-08 17:29:31.974676947 +0000 UTC m=+217.302305912"}]

- GXF profile is not supported by this exporter, since GXF expose its own Rest/SOAP apis specifc to domain.

