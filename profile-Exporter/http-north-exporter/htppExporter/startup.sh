#!/bin/sh

#generate Cert in ssl cert path
mkdir ssl
cd ssl
openssl genrsa -out ca.key 2048
openssl req -new -key ca.key -subj /C=CN/ST=Beijing/L=Biejing/O=edgegallery/CN=edgegallery.org -out ca.csr
openssl x509 -req -days 365 -in ca.csr -extensions v3_ca -signkey ca.key -out ca.crt

openssl genrsa -out server_tls.key 2048
openssl rsa -in server_tls.key -aes256 -passout pass:te9Fmv%qaq -out encryptedtls.key
openssl req -new -key server_tls.key -subj /C=CN/ST=Beijing/L=Biejing/O=edgegallery/CN=edgegallery.org -out tls.csr
openssl x509 -req -days 365 -in tls.csr -extensions v3_usr -CA ca.crt -CAkey ca.key -CAcreateserial -out server_tls.crt

cd -

./app