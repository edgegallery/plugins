/*
 * Copyright 2021 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package controllers

import (
	"encoding/json"
	"fmt"

	"github.com/astaxie/beego"
)

type MainController struct {
	beego.Controller
	FieldOne string `json:"field_one"`
}

func (ctrl *MainController) ProcessData() {
	fmt.Println("In parse function")
	defer ctrl.ServeJSON() // response type

	fmt.Println(string(ctrl.Ctx.Input.RequestBody))

	//var d []model.FledgeData
	var d interface{}
	json.Unmarshal(ctrl.Ctx.Input.RequestBody, &d)

	fmt.Println("Readings from Device:", d)

	//fmt.Println("\nReadings from Device:", d[0].Asset)
	//fmt.Println("\nTime stamp of Readings:", d[0].Timestamp)

	return
}
