### Build

```shell
$ docker build -t swr.ap-southeast-1.myhuaweicloud.com/edgegallery/sampleapp:latest .
```

### K8s command

```shell
$ kubectl apply -f iot-app.yaml

$ kubectl delete -f iot-app.yaml
```
