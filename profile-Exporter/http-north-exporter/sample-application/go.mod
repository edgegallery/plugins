module sample-application

go 1.14

require (
	github.com/astaxie/beego v1.12.3
	github.com/beego/beego/v2 v2.0.1
	github.com/eclipse/paho.mqtt.golang v1.3.5
	github.com/sirupsen/logrus v1.4.2
)
