/*
 * Copyright 2021 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package util

import (
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/libujacob/jsone"
	log "github.com/sirupsen/logrus"
)

// Update tls configuration
func TLSConfig() (*tls.Config, error) {
	return &tls.Config{
		InsecureSkipVerify: true,
	}, nil
}

// Does https request
func DoRequest(req *http.Request) (*http.Response, error) {
	config, err := TLSConfig()
	if err != nil {
		log.Error("Unable to send request")
		return nil, err
	}

	tr := &http.Transport{
		TLSClientConfig: config,
	}
	client := &http.Client{Transport: tr}

	return client.Do(req)
}

func SendConfigToNode(hostUrl string, data string, method string) ([]byte, error) {
	payload := strings.NewReader(data)

	req, err := http.NewRequest(method, hostUrl, payload)
	if err != nil {
		log.Error("New Request error.", err)
		return nil, err
	}

	req.Header.Add("content-type", "application/json")

	response, err := DoRequest(req)
	if err != nil {
		log.Error("Failed to send the request", err)
		return nil, err
	}

	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Error("profile manager response error.", err)
		return nil, err
	}
	fmt.Print("\n-------------------------------------------------------\n")
	fmt.Println(string(body))
	fmt.Print("-------------------------------------------------------\n\n")
	return body, nil
}

func GetValuesByKey(cfg *jsone.O, key string) string {
	var result string = ""
	for _, k := range cfg.Keys() {
		if k == key {
			result, _ = cfg.GetString(key)
		}
	}
	return result
}
func GetDefaultByKey(cfg *jsone.O, key string) *jsone.O {
	for _, k := range cfg.Keys() {
		if k == key {
			nodeObj, err := cfg.GetObject(key)
			if err != nil {
				return nil
			}
			if nodeObj.Has("default") {
				node, _ := nodeObj.GetObject("default")
				return &node
			}
		}
	}
	return nil
}

func GetBrokerHostAndPort(config *jsone.O) (string, int64, error) {
	broker, err := config.GetObject("broker")
	if err != nil {
		log.Errorf(err.Error(), "invalid param while adding south service.")
		return "", 0, err
	}

	brokerHost, _ := broker.GetString("host")
	brokerPort, _ := broker.GetInt64("port")
	return brokerHost, brokerPort, nil
}
